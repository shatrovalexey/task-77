<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group( [
		'as' => 'admin' ,
		'prefix' => 'admin' ,
		'middleware' => [ 'auth' , 'can:admin' ] ,
	] , function ( ) { }
) ;

Auth::routes( ) ;

Route::group( [ 'middleware' => 'auth' , ] , function( ) {
		Route::get( '/' , 'PostController@getList' )->name( 'home' ) ;
		Route::group( [ 'prefix' => '/post' ] , function ( ) {
			Route::get( 'list' , 'PostController@getList' )->name( 'list' ) ;
			Route::get( 'deactivate' , 'PostController@getDeactivate' )->name( 'deactivate' ) ;
			Route::post( 'comment' , 'PostController@postComment' )->name( 'comment' ) ;
		} ) ;
} ) ;