@extends('layouts.app')

@section('content')
<style>
.post-list .strike0 {
	text-decoration: line-through ;
}
.post-form input[type="text"] ,
.post-form textarea {
	width: 100% ;
}
.post-search-form input[type="radio"]:checked ,
.post-search-form input[type="radio"]:checked + span {
	background-color: LightGreen ;
}
</style>
<div class="container">
	@if ( $post_parent )
		@include( 'post.item-parent' , [ 'post_parent' => $post_parent , ] )
		<h2>Список ответов</h2>
	@else
		<h2>Список постов</h2>
		@include( 'post.form.search' , [ 'is_active' => $is_active , 'is_seen' => $is_seen , ] )
	@endif

	@include( 'post.form.input' , [ 'post_parent' => $post_parent , ] )

	<p>Чтобы принять на исполнение пост, необходимо:
	<ul>
		<li>быть администратором</li>
		<li>просто, ответить в посте</li>
	</ul>
	<p>Если администратор1 принял на исполнение какой-то пост, то другим администраторам пост не виден.
	<p>Пользователи видят только свои посты и только свои и администраторов ответы к ним.
	<h3>Список</h3>
	<ul class="post-list">
	@foreach ( $post_list as $post )
		@include( 'post.item' , [ 'post' => $post ] )
	@endforeach
	</ul>

	{{ $post_list->links( ) }}
</div>
@endsection