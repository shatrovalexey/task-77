<div>
	<h2>{{ $post_parent->title }}</h2>
	<time>дата\время создания: {{ $post_parent->created_at }}</time>
	<time>дата\время обновления: {{ $post_parent->updated_at }}</time>
	<span>пользователь: {{ $post_parent->user_name }}</span>

	<div>
		<span>сообщение:</span>
		<div>{{ $post_parent->content }}</div>
	</div>

	<a href="{{ route( 'deactivate' , [ 'post_id' => $post_parent , ] ) }}">дективировать</a>
	<a href="{{ route( 'list' ) }}">список всех постов</a>
</div>