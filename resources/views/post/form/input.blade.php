<form action="{{ route( 'comment' ) }}" method="post" class="post-form" enctype="multipart/form-data">
	<input type="hidden" name="_token" id="csrf-token" value="{{ csrf_token() }}">
	@if ( $post_parent )
	<input type="hidden" name="parent_id" value="{{ $post_parent->id }}">
	@endif

	<fieldset>
		<legend>
			@if ( $post_parent )
			<h2>Создать ответ</h2>
			@else
			<h2>Создать пост</h2>
			@endif
		</legend>
		<label>
			<input type="text" name="title" required placeholder="заголовок">
		</label>
		<label>
			<textarea name="content" required placeholder="сообщение"></textarea>
		</label>
		<label>
			<input type="file" name="file" placeholder="файл">
		</label>
		<label>
			<span>сохранить</span>
			<input type="submit" value="&rarr;">
		</label>
	</fieldset>
</form>