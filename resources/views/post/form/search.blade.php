<form action="{{ route( 'list' ) }}" class="post-search-form">
	<fieldset>
		<legend>
			<h2>Поиск</h2>
		</legend>
		<label>
			<input type="radio" name="is_seen" value="1"
			@if ( $is_seen )
				checked
			@endif
			>
			<span>просмотренные</span>
		</label>
		<label>
			<input type="radio" name="is_seen" value="0"
			@if ( ! $is_seen )
				checked
			@endif
			>
			<span>непросмотренные</span>
		</label>
		<label>
			<input type="radio" name="is_active" value="1"
			@if ( $is_active )
				checked
			@endif
			>
			<span>активные</span>
		</label>
		<label>
			<input type="radio" name="is_active" value="0"
			@if ( ! $is_active )
				checked
			@endif
			>
			<span>неактивные</span>
		</label>
		<label>
			<input type="submit" value="&rarr;">
		</label>
	</fieldset>
</form>