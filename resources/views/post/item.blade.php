<li class="strike{{$post->is_active}}">
	<time>дата\время создания: {{ $post->created_at }}</time>
	<time>дата\время обновления: {{ $post->updated_at }}</time>
	<span>пользователь: {{ $post->user_name }}</span>
	<div>заголовок: {{ $post->title }}</div>
	@if ( $post->file_name )
	<div>файл: <a href="/uploads/{{ $post->file_name }}">/uploads/{{ $post->file_name }}</a></div>
	@endif
	<a href="{{ route( 'deactivate' , [ 'post_id' => $post->id , ] ) }}">дективировать</a>
	<a href="{{ route( 'list' , [ 'parent_id' => $post->id , ] ) }}">комментировать</a>
	<br>
	<br>
</li>