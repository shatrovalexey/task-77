@extends('layouts.app')

@section('content')
<div class="flex-center position-ref full-height">
	<div class="top-right links">
		<a href="{{ route('/auth/login') }}">Login</a>
		<a href="{{ route('/auth/register') }}">Register</a>
	</div>
</div>
@endsection