<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\User ;
use App\Mail\Mailer ;

class Post extends Model {
	protected $table = 'post' ;
	protected $fillable = [ 'is_active' , 'deactivated_at' , 'title' , 'comment' , 'user_id' , 'post_id' , 'file_name' ] ;

	/**
	* список
	*
	* @param App\User $user - идентификатор пользователя
	* @param array $args - дополнительные аргументы для поиска
	* - integer $parent_id - родительский пост
	* - integer $post_id - идентификатор поста
	* - boolean $is_seen - пост просмотрен
	* - boolean $is_active - пост активен\не активен
	*
	* @return Illuminate\Database\Eloquent\Collection
	*/
	public static function getList( $user , $args = [ ] ) {
		$result = DB::table( 'post AS p1' )
			->select( 'p1.*' , 'u1.name AS user_name' )
			->join( 'users AS u1' , 'p1.user_id' , '=' , 'u1.id' ) ;

		if ( isset( $args[ 'is_active' ] ) ) {
			$result->where( 'p1.is_active' , ( int )@$args[ 'is_active' ] ) ;
		}

		if ( ! $user->isAdmin( ) ) {
			$result->where( 'p1.user_id' , $user->id ) ;
		}
		if ( ! empty( $args[ 'post_id' ] ) ) {
			$result->where( 'p1.post_id' , $args[ 'post_id' ] ) ;
		}
		if ( isset( $args[ 'is_active' ] ) ) {
			if ( empty( $args[ 'is_seen' ] ) ) {
				$result->leftJoin( 'user_post_seen AS ups1' , function ( $join ) use( $user ) {
					$join->on( 'ups1.post_id' , '=' , 'p1.id' )
						->where( 'ups1.user_id' , $user->id ) ;
				} )->where( 'ups1.user_id' )
					->where( 'p1.manager_id' ) ;
			} else {
				$result->join( 'user_post_seen AS ups1' , 'ups1.post_id' , '=' , 'p1.id' )
					->where( 'ups1.user_id' , $user->id ) ;
			}
		}

		$result->orderBy( 'p1.created_at' , 'desc' ) ;

		return $result ;
	}

	/**
	* Создать пост или комментарий к посту
	*
	* @param App\User $user - пользователь
	* @param string $title - заголовок
	* @param string $content - комментарий
	* @param integer $parent_id - идентификатор родительского поста
	*/
	public static function addComment( $user , $title , $content , $parent_id = null , $file_name = null ) {
		if ( ! empty( $parent_id ) ) {
			$parent = static::where( 'id' , $parent_id ) ;

			if ( ! $user->isAdmin( ) ) {
				$parent->where( 'user_id' , $user->id ) ;
			}
			if ( ! $parent->first( ) ) {
				abort( 404 ) ;
			}
		}

		$post = new static( ) ;

		if ( $user->isAdmin( ) ) {
			$post->manager_id = $user->id ;
		}

		$post->user_id = $user->id ;
		$post->post_id = $parent_id ;
		$post->title = $title ;
		$post->content = $content ;
		$post->file_name = $file_name ;
		$post->save( ) ;

		if ( empty( $parent_id ) ) {
			return $post ;
		}

		if ( $user->isAdmin( ) ) {
			$post = static::where( 'id' , $parent_id  )->first( ) ;
			$post->manager_id = $user->id ;
			$post->update( ) ;
		}

		$contragent_id = $user->id ;

		if ( ! $user->isAdmin( ) ) {
			$contragent_id = static::select( 'manager_id' )
				->where( 'id' , $parent_id )
				->one( ) ;
		}

		DB::table( 'user_post_seen' )
			->where( 'user_id' , $contragent_id )
			->where( 'post_id' , $parent_id )
			->delete( ) ;

		Mailer::notifyUser( $contragent_id , $parent_id ) ;

		return $post ;
	}

	/**
	* установить активность записи
	*
	* @param App\User $user - пользователь
	* @param integer $post_id - идентификатор поста
	* @param boolean $value - активно\не активно
	*
	* @return Illuminate\Database\Eloquent\Collection
	*/
	public static function setIsActive( \App\User $user , $post_id , $value = false ) {
		$post = static::where( 'id' , $post_id ) ;

		if ( ! $user->isAdmin( ) ) {
			$post->where( 'user_id' , $user->id ) ;
		}

		$post = $post->first( ) ;
		$post->is_active = $value ;
		if ( empty( $value ) ) {
			$post->deactivated_at = \Carbon\Carbon::now( ) ;
		} else {
			$post->deactivated_at = null ;
		}
		$post->save( ) ;

		return $post ;
	}

	/**
	* установить просмотренность записи
	*
	* @param App\User $user - пользователь
	* @param integer $post_id - идентификатор поста
	* @param boolean $value - видел\не видел
	*
	* @return Illuminate\Database\Eloquent\Collection
	* @throws \Exception
	*/
	public static function setIsSeen( \App\User $user , $post_id , $value = true ) {
		$post = static::where( 'id' , $post_id ) ;

		if ( ! $user->isAdmin( ) ) {
			$post->where( 'user_id' , $user->id ) ;
		}

		$post = $post->first( ) ;
		$post->is_seen = $value ;
		$post->save( ) ;

		try {
			DB::table( 'user_post_seen' )
				->insert( [
					'user_id' => $user->id ,
					'post_id' => $post->id ,
					'created_at' => \Carbon\Carbon::now( ) ,
					'updated_at' => \Carbon\Carbon::now( ) ,
				] ) ;
		} catch ( \Exception $exception ) {
			return null ;
		}

		return $post ;
	}
}