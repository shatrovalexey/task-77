<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request ;
use Illuminate\Support\Facades\DB ;
use App\Post;

class PostController extends Controller {
	/**
	*  Деактивация поста
	*
	* @param integer $post_id - идентификатор поста
	*/
	public function getDeactivate( ) {
		$post_id = request( )->get( 'post_id' ) ;
		$user = auth( )->user( ) ;
		Post::setIsActive( $user , $post_id ) ;

		return redirect( )->back( ) ;
	}

	/**
	* Создать пост или комментарий к посту
	*
	* @param App\User $user - пользователь
	* @param string $title - заголовок
	* @param string $content - комментарий
	* @param integer $parent_id - идентификатор родительского поста
	*/
	public function postComment( ) {
		$request = request( ) ;
		$user = auth( )->user( ) ;

		$parent_id = $request->post( 'parent_id' ) ;
		$title = $request->post( 'title' ) ;
		$content = $request->post( 'content' ) ;
		$file_name = null ;

		if ( $request->hasFile( 'file' ) ) {
			$file = $request->file( 'file' ) ;

			if ( $file->isValid( ) ) {
				$file_name = sprintf( '%s.%s' , sha1( uniqid( ) ) , $file->extension( ) ) ;
				$file->move( public_path( ) . '/uploads' , $file_name ) ;
			}
		}

		$post = Post::addComment( $user , $title , $content , $parent_id , $file_name ) ;

		return redirect( )->back( ) ;
	}

	/**
	*  Список постов
	*
	* @param integer $parent_id - идентификатор поста
	* @param boolean $is_seen - пост просмотрен
	* @param boolean $is_active - пост активен\не активен
	*/
	public function getList( ) {
		$parent_id = request( )->get( 'parent_id' ) ;
		$is_active = request( )->get( 'is_active' ) ; // по-умолчанию = 1
		$is_seen = request( )->get( 'is_seen' ) ; // по-умолчанию = 0
		$user = auth( )->user( ) ;

		if ( is_null( $is_active ) ) {
			$is_active = true ;
		} else {
			$is_active = ( int ) $is_active ;
		}

		if ( is_null( $is_seen ) ) {
			$is_seen = false ;
		} else {
			$is_seen = ( int ) $is_seen ;
		}

		if ( ! empty( $parent_id ) ) {
			$post_parent = Post::getList( $user , [
				'parent_id' => $parent_id ,
			] ) ;

			$post_parent = $post_parent->first( ) ;

			if ( $post_parent ) {
				Post::setIsSeen( $user , $post_parent->id ) ;
			}
		}

		$post_list = Post::getList( $user , [
			'post_id' => $parent_id ,
			'is_seen' => $is_seen ,
			'is_active' => $is_active ,
		] ) ;

		$post_list = $post_list->paginate( 5 ) ;

		if ( ! empty( $post_parent ) ) {
			foreach ( $post_list as $post ) {
				Post::setIsSeen( $user , $post->id ) ;
			}
		}

		return view( 'post.list' , [
			'is_seen' => $is_seen ,
			'is_active' => $is_active ,
			'post_list' => $post_list ,
			'post_parent' => @$post_parent ,
		] ) ;
	}
}