<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\User ;
use Illuminate\Support\Facades\Mail ;

class Mailer extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct( ) {
    }

	/**
	* Подготовка письма котрагенту
	*/
	public static function build( ) {
		return $this
			->view( 'mails.notification_html' )
			->text( 'mails.notification_plain' ) ;
	}

	/**
	* Отправка письма котрагенту
	*
	* @param integer $user_id - идентификатор пользователя
	* @param integer $post_id - идентификатор поста
	*/
    public static function notifyUser( $post_id , $user_id ) {
		$user = User::where( 'id' , $user_id )->first( ) ;
		$post = Post::where( 'id' , $post_id )->first( ) ;

		$data = new stdclass( ) ;
		$data->user = $user ;
		$data->post_id = $post ;

		return Mail::to( $user->email )->send( new Mailer( $data ) ) ;
	}
}