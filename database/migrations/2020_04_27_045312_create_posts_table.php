<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePostsTable extends Migration {
	/**
	* Run the migrations.
	*
	* @return void
	*/
	public function up( ) {
		Schema::create( 'post' , function ( Blueprint $table ) {
			$table->bigIncrements( 'id' ) ;
			$table->bigInteger( 'post_id' )->unsigned( )->nullable( ) ;
			$table->bigInteger( 'user_id' )->unsigned( ) ;
			$table->bigInteger( 'manager_id' )->unsigned( )->nullable( ) ;
			$table->longText( 'content' ) ;
			$table->string( 'title' ) ;
			$table->tinyInteger( 'is_active' )->unsigned( )->default( true ) ;
			$table->tinyInteger( 'is_seen' )->unsigned( )->default( true ) ;
			$table->string( 'file_name' )->nullable( ) ;
			$table->timestamp( 'deactivated_at' ) ;
			$table->timestamps( ) ;

			$table->foreign( 'user_id' )
				->references( 'id' )->on( 'users' )->onDelete( 'cascade' ) ;
			$table->foreign( 'manager_id' )
				->references( 'id' )->on( 'users' )->onDelete( 'cascade' ) ;
			$table->foreign( 'post_id' )
				->references( 'id' )->on( 'post' )->onDelete( 'cascade' ) ;
			$table->index( [ 'created_at' , 'is_active' ] ) ;
		} ) ;

		Schema::create( 'user_post_seen' , function ( Blueprint $table ) {
			$table->bigInteger( 'user_id' )->unsigned( ) ;
			$table->bigInteger( 'post_id' )->unsigned( ) ;

			$table->primary( [ 'user_id' , 'post_id' , ] ) ;

			$table->foreign( 'user_id' )
				->references( 'id' )->on( 'users' )->onDelete( 'cascade' ) ;
			$table->foreign( 'post_id' )
				->references( 'id' )->on( 'post' )->onDelete( 'cascade' ) ;

			$table->timestamps( ) ;
		} ) ;
	}

	/**
	* Reverse the migrations.
	*
	* @return void
	*/
	public function down( ) {
		Schema::dropIfExists( 'user_post_seen' ) ;
		Schema::dropIfExists( 'post' ) ;
	}
}
